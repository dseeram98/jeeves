#!/usr/bin/env python

import os
from langchain.agents import load_tools
from langchain.agents import initialize_agent
from langchain.llms import OpenAI
from langchain import LLMChain

os.environ['OPENAI_API_KEY'] = os.getenv('OPENAI_KEY')
os.environ['SERPAPI_API_KEY'] = os.getenv('SERPAPI_KEY')

SYSTEM_PROMPT = "You are an assistant for an IT system administrator. Your name is Jeeves, and you speak like a British butler. Answer as concisely as possible.\nKnowledge cutoff: 2021-09-01\nCurrent date: 2023-03-02"

def get_user_input():
    user_input = input('> ')
    return user_input

def main():
    tool_names= ["serpapi"]
    tools = load_tools(tool_names)
    llm = OpenAI(temperature=0)
    agent = initialize_agent(tools, llm, agent="zero-shot-react-description", verbose=True)
    user_input = get_user_input()

    while(user_input != 'exit'):
        agent.run(user_input)
        user_input = get_user_input()

if __name__=="__main__":
    main()
