#!/usr/bin/env python

import openai
import os
from datetime import datetime
import argparse

parser = argparse.ArgumentParser(description='CLI Personal Assistant leveraging the OpenAI API')

openai.api_key = os.getenv('OPENAI_KEY')

SYSTEM_PROMPT = "You are an assistant for an IT system administrator. Your name is Jeeves, and you speak like a British butler. Answer as concisely as possible.\nKnowledge cutoff: 2021-09-01\nCurrent date: 2023-03-02"

def get_user_input():
    user_prompt = input('> ')
    return {"role": "user", "content" : user_prompt}

def get_response(message_payload):
    completion = openai.ChatCompletion.create(
        model="gpt-3.5-turbo", 
        messages=message_payload,
        max_tokens=1024
    )
    response = completion['choices'][0]['message']['content']
    return response

def main():
    user_prompt = get_user_input()
    message_payload = [
                {"role": "system", "content" : SYSTEM_PROMPT},
               ]
    while(user_prompt['content'].lower() != 'exit'):
        message_payload.append(user_prompt)
        response = get_response(message_payload)
        print(f"\033[95m {response}\033[00m")
        message_payload.append({"role": "assistant", "content": response})
        user_prompt = get_user_input()

if __name__=="__main__":
    main()
